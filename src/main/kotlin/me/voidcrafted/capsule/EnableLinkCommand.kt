package me.voidcrafted.capsule
import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import com.jagrosh.jdautilities.commons.waiter.EventWaiter
import net.dv8tion.jda.core.entities.ChannelType


class EnableLinkCommand(private val waiter: EventWaiter) : Command() {
    init {
        this.name = "enablelink"
        this.aliases = arrayOf("link")
        this.guildOnly = true
        this.ownerCommand = true
        this.help = "starts a link with -link <number> <contact name>"
    }

    override fun execute(event: CommandEvent) {
        if (event.args.split(' ').size < 2)
            return event.reply("You need to include the number & contact name, like -link 447443870698 Jay Williams")
        var number = event.args.split(' ')[0]
        var name = event.args.substringAfter(' ')
        LinkStorage.linkedChannels[name] = ChannelLink(event.textChannel, name, number )
    }
}
