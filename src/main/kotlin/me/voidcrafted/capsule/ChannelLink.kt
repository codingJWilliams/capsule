package me.voidcrafted.capsule

import net.dv8tion.jda.core.entities.TextChannel

class ChannelLink (val channel : TextChannel, val username : String, val number : String)