package me.voidcrafted.capsule

import me.voidcrafted.pushbullet.EventStream
import org.json.JSONObject

class PushbulletListener(token : String) : EventStream(token) {
    val acceptableTypes = arrayOf("push")
    override fun onMessage(message: String?) {
        val messageObject = JSONObject(message)
        // If it isn't a type we're listening for, ignore
        if (!acceptableTypes.contains(messageObject.getString("type"))) return

        // Get push subsection
        val push = messageObject.getJSONObject("push")

        // If it's not a messenger notification, ignore
        if (push.getString("type") != "mirror") return
        if (push.getString("application_name") != "Messenger") return

        println(push.getString("title"))
        val link = LinkStorage.linkedChannels[push.getString("title")]
          ?: return
        val messageToSend = "**${link.username}**: ${push.getString("body")}"
        println(messageToSend)
        link.channel.sendMessage(messageToSend).queue()
    }
}