package me.voidcrafted.capsule

import org.json.JSONObject
import java.io.File

fun main (args : Array<String>) {
    val config = JSONObject(File("./config.json").readText())

    // Start listening
    DiscordSetup(config.getString("token"))
    PushbulletListener(config.getString("pushbullet"))
}

