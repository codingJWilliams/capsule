package me.voidcrafted.capsule

import com.jagrosh.jdautilities.command.CommandClientBuilder
import com.jagrosh.jdautilities.commons.waiter.EventWaiter
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.OnlineStatus
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import org.json.JSONObject
import java.io.File


class DiscordSetup (token : String) : ListenerAdapter() {
    private val config = JSONObject(File("./config.json").readText())
    private val waiter = EventWaiter()
    private val client = CommandClientBuilder()
            .useDefaultGame()
            .setOwnerId("193053876692189184")
            .setPrefix("-")
            .setEmojis("\uD83D\uDE03", "\uD83D\uDE2E", "\uD83D\uDE26")
            .addCommands(
                    EnableLinkCommand(waiter)
            )

    private val jda = JDABuilder(AccountType.BOT)
            .setToken(token)
            .setStatus(OnlineStatus.IDLE)
            .addEventListener(waiter)
            .addEventListener(client.build())
            .buildAsync()

    private val pushSender = PushbulletSender(config.getString("pushbullet"), config.getString("device"))
    init {
        jda.addEventListener(this)
    }

    override fun onMessageReceived (message : MessageReceivedEvent) {
        if (message.message.contentRaw.toLowerCase().startsWith("-link")) return
        if (message.author.id == jda.selfUser.id) return
        LinkStorage.linkedChannels
                .filter { (_, v) -> v.channel.id == message.channel.id }
                .forEach { (_, v) ->
                    pushSender.sendSms(v.number, "[#${message.channel.name} - ${message.author.name}] ${message.message.contentDisplay}")
                    println("${v.number} <-- [#${message.channel.name} - ${message.author.name}] ${message.message.contentDisplay}")
                }
    }
}
